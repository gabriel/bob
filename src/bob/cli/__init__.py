# SPDX-FileCopyrightText: 2022-present Gabriel Duque <gabriel@duque.bzh>
#
# SPDX-License-Identifier: MIT

from bob.__about__ import __version__


def bob() -> int:
    print("Bob the builder!")
    return 0
